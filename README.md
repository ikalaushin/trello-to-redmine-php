# Trello to Redmine
This is a tiny tool I've re-written to migrate all of our issue tracking data at once. It uses Trello's JSON API feature and the JSON flavour of [Redmine's REST API](http://www.redmine.org/projects/redmine/wiki/Rest_api).
Original Python version can be found [here](https://github.com/inequation/trello2redmine)


## Dependencies

Tested with PHP 5.6.20.
Requires CURL extension


## Configuration and usage

Edit [t2r_conf.php](t2r_conf.php) and fill in the details of your installation. Then just run the script in your console.
Additionally you can edit Attachments section of [t2r.php](t2r.php) to change phrases about attachments

## Known issues

Not all features of Trello are supported. What works:

* creating Redmine issues from Trello cards,
* importing Trello card comments as Redmine issue comments (with author names and date-times),
* importing Trello checklists as plain text, appended to the issue description,
* mapping Trello users to Redmine users,
* mapping Trello lists to Redmine statuses,
* mapping Trello card labels to Redmine priorities.
* multiple Trello card assignees to One (first) Redmine assignee ans other as spectators
* attachments are downloading from Trello and uploading to Redmine (with author names and date-times), but it makes script SLOW

What doesn't:

* everything else.

## Licensing

```
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 3, December 2016 

 Copyright (C) 2016 Ivan Kalaushin

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
```