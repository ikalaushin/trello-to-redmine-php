#!/usr/bin/php
<?php
/**
 * t2r.php
 *
 * Trello to Redmine migrating script
 *
 * Created by PhpStorm.
 * User: ikalaushin (ikalaushin@gmail.com)
 * Date: 02.12.2016
 * Time: 17:06
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(0); 
ini_set('memory_limit', '1024M');

$cfg = array();
require_once './t2r_conf.php';
global $cfg;
# ============================================================================
# Functions
# ============================================================================

/**
 * Method for checking curl-methods availability.
 *
 *
 * @return bool
 */
function cURLcheckBasicFunctions()
{
    if( !function_exists("curl_init") &&
        !function_exists("curl_setopt") &&
        !function_exists("curl_exec") &&
        !function_exists("curl_close") ) return false;
    else return true;
}

/**
 * Method for making GET-query by CURL
 *
 * @param       $url
 *
 * @return string
 */
function curl_get($url){
    if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions";
    global $cfg;
    $ch = curl_init();

$header = array("X-Redmine-API-Key: ". $cfg['redmine_api_key'] . "
Content-Type: application/json
Accept: application/json");

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);

    if(curl_errno($ch)){
        print("\n\n" . curl_error($ch) . "\n");
        die(1);
    }

    curl_close($ch);
    unset($ch);
    return $output;
}

/**
 * Method for making POST-query by CURL
 *
 * @param       $url
 * @param       $data
 *
 * @return string
 */
function curl_post($url, $data = array()){
    if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions";
    global $cfg;

    $ch = curl_init();

    $header = array();
    $header[] = "Accept: application/json";
    $header[] = "Content-Type: application/json";
    $header[] = "X-Redmine-API-Key: ". $cfg['redmine_api_key'];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);


    if($data){
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);

    if(curl_errno($ch)){
        print("\n\n" . curl_error($ch) . "\n");
        print("    Responce get:\n");
        var_dump($output);
        print("    Data sent:\n");
        var_dump($data);
        print("\n");
        die(1);
    }

    curl_close($ch);
    unset($ch);
    return $output;
}

/**
 * Method for file sending POST-query by CURL
 *
 * @param       $url
 * @param       $data
 *
 * @return string
 */
function curl_post_file($url, $data = array()){
    if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions";
    global $cfg;

    $ch = curl_init();

    $header = array();
    $header[] = "Accept: application/json";
    $header[] = "Content-Type: application/octet-stream";
    $header[] = "X-Redmine-API-Key: ". $cfg['redmine_api_key'];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);


    if($data){
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);

    if(curl_errno($ch)){
        print("\n\n" . curl_error($ch) . "\n");
        print("    Responce get:\n");
        var_dump($output);
        print("    Data size sent: " . strlen($data) . "bytes \n");
        print("\n");
        die(1);
    }

    curl_close($ch);
    unset($ch);
    return $output;
}

/**
 * Method for making PUT-query by CURL
 *
 * @param       $url
 * @param       $data
 * @param array $header
 *
 * @return string
 */
function curl_put($url, $data = array()){
    if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions";
    global $cfg;

    $ch = curl_init();

    $header = array();
    $header[] = "Accept: application/json";
    $header[] = "Content-Type: application/json";
    $header[] = "X-Redmine-API-Key: ". $cfg['redmine_api_key'];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    if($data){
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);

    if(curl_errno($ch)){
        print("\n\n" . curl_error($ch) . "\n");
        print("    Responce get:\n");
        var_dump($output);
        print("    Data sent:\n");
        var_dump($data);
        print("\n");
        die(1);
    }

    curl_close($ch);
    unset($ch);
    return $output;
}
/**************************************************************************************************************************************************************/
# ============================================================================
# Logic Starts Here
# ============================================================================

if(!isset($argv)) $argv = array();
if( count($argv) > 2 or (count($argv) == 1 and $argv[0] == '-h')){
    print('Usage: ' .$argv[0] ." [<options>]) \n");
    print("See t2r_conf.php for configuration.\n");
    print("Options:\n");
    print(" -h    Displays this information.\n");
    print(" -c    Commits the import. Otherwise does a dry run (prints resulting JSON to screen instead of sending it to Redmine).\n");
    die(0);
}

# If a dry run, JSON is printed instead of submitted to Redmine.
$dry_run = count($argv) < 2 or $argv[1] != '-c';
if ($dry_run){
    print("Making a dry run! Re-run with -c to commit the import into Redmine.\n");
} else {
    print("Making a commit!\n");
}

$url = $cfg['trello_api_link'] . '/boards/' . $cfg['trello_board_id'] . '?' . $cfg['trello_params'] . '&token=' . $cfg['trello_token'] . '&key=' . $cfg['trello_key'];

if (!strstr($url, 'trello.com')){
    print("URL $url does not seem to be a Trello board link. Are you sure this is correct? \n");
    die(1);
}

# ============================================================================
# Trello JSON processing starts here.
# ============================================================================
print("\n\n┌──────────────────────────────────────────────────────────────────────────┐\n");
print("│                           Download Trello Board                          │\n");
print("└──────────────────────────────────────────────────────────────────────────┘\n");
print("Downloading board JSON from Trello... \n");

$board = json_decode(curl_get($url));
print("Processing board JSON...\n");

// Lists

$orig_lists_dict = array();
$lists_dict = array();
foreach ($board->lists as $l){
    $name = $l->name;
    if (in_array($name, $cfg['list_map'])){
        $name = $cfg['list_map'][$name];
    }
    $lists_dict[$l->id] = $name;
    $orig_lists_dict[$l->id] = $l->name;
}

// CheckLists

$checklists_dict = array();
foreach ($board->checklists as $c) {
    $checklists_dict[$c->id]['checkItems'] = $c->checkItems;
    $checklists_dict[$c->id]['name'] = $c->name;
}
// Members

$members_dict = array();
foreach ($board->members as $m) {
    $name = $m->fullName;
    if (in_array($name ,$cfg['username_map'])){
        $name = $cfg['username_map'][$name];
    } 

    $members_dict[$m->id] = $name;
}

 // Labels

$labels_dict = array();
foreach ($board->labels as $l) {
    $label = $l->name;
    if (in_array($label, $cfg['label_map'])) {
        $label = $cfg['label_map'][$label];
    }
    $labels_dict[$l->id] = $label;

}

$cardsCount = count($board->cards);
print("Got: \n");
print("    Board name: '" . $board->name .'\'');
print("\n    Members: " . count($members_dict));
print("\n    Lists: " . count($lists_dict));
print("\n    Labels: " . count($labels_dict));
print("\n    CheckLists: " . count($checklists_dict));
print("\n    Cards: " . $cardsCount);

// ============================================================================
// Redmine configuration processing starts here.
// ============================================================================
print("\n\n┌──────────────────────────────────────────────────────────────────────────┐\n");
print("│                           Connecting to Redmine                          │\n");
print("└──────────────────────────────────────────────────────────────────────────┘\n");
print("Querying Redmine configuration...\n");

// ===== Projects =====

$redmine_projects = curl_get($cfg['redmine_root_url'] . '/projects.json');
if($redmine_projects){
    $redmine_projects = json_decode($redmine_projects);
}

$redmine_project_id = -1;
   
foreach($redmine_projects->projects as $rp){
    if ($rp->identifier == $cfg['redmine_project_identifier']){
        $redmine_project_id = $rp->id;
        $redmine_project_name = $rp->name;
        break;
    }
}

if ($redmine_project_id < 0){
    print('Project with identifier ' . $cfg['redmine_project_identifier'] . " does not exist in Redmine!\n\n");
    die(1);
}

// ===== Users =====

$redmine_users = curl_get($cfg['redmine_root_url'] . '/users.json');
if($redmine_users){
    $redmine_users = json_decode($redmine_users);
}

$redmine_users_dict = array();

foreach ($redmine_users->users as $ru) {
    $fullname = $ru->firstname . ' ' . $ru->lastname;
    $redmine_users_dict[$fullname] = $ru->id;
}

if (!isset($redmine_users_dict[$cfg['redmine_default_user']])) {
    print("Default user does not exist in Redmine!\n\n");  var_dump($redmine_users_dict);
    die(1);
}

// ===== Priorities =====

$redmine_priorities = curl_get($cfg['redmine_root_url'] . '/enumerations/issue_priorities.json');
if($redmine_priorities){
    $redmine_priorities = json_decode($redmine_priorities);
}
$redmine_priorities_dict = array();

$redmine_default_priority = "Нормальный";
foreach ($redmine_priorities->issue_priorities as $rp) {
    $redmine_priorities_dict[$rp->name] = $rp->id;
    if (isset($rp->is_default) AND $rp->is_default){
        $redmine_default_priority = $rp->name;
    }
}

// ===== Statuses =====

$redmine_statuses = curl_get($cfg['redmine_root_url'] . '/issue_statuses.json');
if($redmine_statuses ){
    $redmine_statuses  = json_decode($redmine_statuses);
}
$redmine_statuses_dict = array();

$redmine_default_status = $cfg['redmine_default_status'];
foreach ($redmine_statuses->issue_statuses as $rs) {
    $redmine_statuses_dict[$rs->name] = $rs->id;
    if (isset($rs->is_default) and $rs->is_default){
        $redmine_default_status = $rs->name;
    }
}

print("Got: \n");
print("    Progect name: '$redmine_project_name''");
print("\n    Users: " . count($redmine_users_dict));
print("\n    Statuses: " . count($redmine_statuses_dict));
print("\n    Prioriries: " . count($redmine_priorities_dict));

// ============================================================================
// Direct Trello-to-Redmine mappings are made here.
// ============================================================================

print("\n\n┌──────────────────────────────────────────────────────────────────────────┐\n");
print("│                      Direct Trello-to-Redmine Mapping                    │\n");
print("└──────────────────────────────────────────────────────────────────────────┘\n");
print("Generating configuration mappings...");

print("\n\nUsers:\n");
$user_map = array();
foreach ($members_dict as $id => $fullname) {
    if(! isset($cfg['username_map'][$fullname]) OR !isset($redmine_users_dict[$cfg['username_map'][$fullname]])){
        print("WARNING: user '$fullname' not found in Redmine, defaulting to '{$cfg['redmine_default_user']}'\n");
        $fullname = $cfg['redmine_default_user'];
        $redmine_id = $redmine_users_dict[$fullname];
    }else{
        $redmine_id = $redmine_users_dict[$cfg['username_map'][$fullname]];
    }
    $user_map[$id] = $redmine_id;
    print("    Matched user '$fullname' <=> '" . (isset($cfg['username_map'][$fullname]) ? $cfg['username_map'][$fullname] : '') . "', Trello ID '$id' <=> Redmine ID '$redmine_id'\n");
}

print("\nPriorities:\n");
$priority_map = array();
foreach ($labels_dict as $id => $name ) {
    if (!isset($cfg['label_map'][$name]) OR !isset($redmine_priorities_dict[$cfg['label_map'][$name]])) {
        print("WARNING: Trello label '$name' is not mapped to a Redmine priority, defaulting to '$redmine_default_priority'\n");
        $name = $redmine_default_priority;
        $redmine_id = $redmine_priorities_dict[$name];
    }else{
        $redmine_id = $redmine_priorities_dict[$cfg['label_map'][$name]];
    }
    $priority_map[$id] = $redmine_id;
    print("    Matched label '$name' <=> '" . (isset($cfg['label_map'][$name]) ? $cfg['label_map'][$name] : '') . "', Trello id '$id' <=> Redmine id '$redmine_id'\n");
}

print("\nStatuses:\n");
$status_map = array();
foreach ($lists_dict as $id => $name) {
    if (!isset($cfg['list_map'][$name]) OR !isset($redmine_statuses_dict[$cfg['list_map'][$name]])) {
        print("WARNING: Trello list '$name' is not mapped to a Redmine status, defaulting to '$redmine_default_status'\n");
        $name = $redmine_default_status;
        $redmine_id = $redmine_statuses_dict[$name];
    }else{
        $redmine_id = $redmine_statuses_dict[$cfg['list_map'][$name]];
    }
    $status_map[$id] = $redmine_id;
    print("    Matched list '$name' <=> '" . (isset($cfg['list_map'][$name]) ? $cfg['list_map'][$name] : '') . "', Trello id '$id' <=> Redmine id '$redmine_id'\n");
}


# ============================================================================
# Finally, cards processing.
# ============================================================================
print("\n\n┌──────────────────────────────────────────────────────────────────────────┐\n");
print("│                          Trello-to-Redmine Import                        │\n");
print("└──────────────────────────────────────────────────────────────────────────┘\n");
print("Processing cards...");


$dry_run_counter = 0;
$cardsCount = count($board->cards);
foreach ($board->cards as $cardIndex => $crd) {

    // Add checkLists into task description
    $desc = $crd->desc;
    if ($crd->idChecklists){
        $desc .= "\n";
        foreach($crd->idChecklists as $id) {
            $desc .= "\n\n## [" . ($checklists_dict[$id]['name'] ? $checklists_dict[$id]['name'] : 'Контрольный список') . "]\n";
            foreach($checklists_dict[$id]['checkItems'] as $item) {
                $desc .= "\n* [" . ($item->state == 'complete' ? 'x' : ' ') . "] {$item->name}";
            }
        }
    }

    // Add Trello Labels into task description
    if ($crd->idLabels){
        $labels = '';
        foreach($crd->idLabels as $id) {
            if($labels_dict[$id]){
                $labels .= "**[ " . $labels_dict[$id] ." ]**, ";
            }
        }

        if($labels){
            $desc = "## Trello Labels: \n" . $labels . "\n***\n";
        }
    }

    $issue = new stdClass();
    $issue->subject = "[{$orig_lists_dict[$crd->idList]}] {$crd->name}";
    // Change subject length to safe if needed
    if(mb_strlen($issue->subject) > 250){
        $desc = $issue->subject . "\n\n" . $desc;
        $issue->subject = mb_substr($issue->subject,0, 150) . '...';
    }
    $desc = "### [Оригинал карточки в Trello]({$crd->url})\n\n" . $desc;
    $issue->description = $desc;
    $issue->project_id  = $redmine_project_id;
    $issue->assigned_to_id = ($crd->idMembers ? $user_map[$crd->idMembers[0]] : "none");

    $issue->priority_id = ($crd->idLabels ? $priority_map[$crd->idLabels[0]] : $redmine_priorities_dict[$redmine_default_priority]);
    $issue->is_private = false;

    // Spectators from multiple members
    if($crd->idMembers and isset($crd->idMembers) and is_array($crd->idMembers)){
        $issue->assigned_to_id = $user_map[$crd->idMembers[0]];

        if(count($crd->idMembers) > 1) { // Making other members an watchers
            $issue->watcher_user_ids = array();
            for($memberIterator = 1; $memberIterator < count($crd->idMembers); $memberIterator++){
                $issue->watcher_user_ids[] = $user_map[$crd->idMembers[$memberIterator]];
            }
        }
    }else{
        $issue->assigned_to_id = "none";
    }

    // Trying to guess tracker for task base on card name
    $issue->tracker_id = $cfg['redmine_default_tracker_id'];
    if(isset($cfg['redmine_tracker_error_alias']) and is_array($cfg['redmine_tracker_error_alias']) and count($cfg['redmine_tracker_error_alias'])) {
        foreach ($cfg['redmine_tracker_error_alias'] as $alias) {
            if (mb_strpos(mb_strtolower($crd->name), mb_strtolower($alias)) !== false) {
                $issue->tracker_id = 1;
                break;
            }
        }
    }

    if($issue->tracker_id == $cfg['redmine_default_tracker_id'] and isset($cfg['redmine_tracker_feature_alias']) and is_array($cfg['redmine_tracker_feature_alias']) and count($cfg['redmine_tracker_feature_alias'])) {
        foreach ($cfg['redmine_tracker_feature_alias'] as $alias) {
            if (mb_strpos(mb_strtolower($crd->name), mb_strtolower($alias)) !== false) {
                $issue->tracker_id = 2;
                break;
            }
        }
    }

    if($issue->tracker_id == $cfg['redmine_default_tracker_id'] and isset($cfg['redmine_tracker_support_alias']) and is_array($cfg['redmine_tracker_support_alias']) and count($cfg['redmine_tracker_support_alias'])) {
        foreach ($cfg['redmine_tracker_support_alias'] as $alias) {
            if (mb_strpos(mb_strtolower($crd->name), mb_strtolower($alias)) !== false) {
                $issue->tracker_id = 3;
                break;
            }
        }
    }

    $card = new stdClass();
    $card->issue = $issue;

    print("\n}-- " . ($cardIndex + 1) . " / $cardsCount -->");
    print("\n    Importing '{$card->issue->subject}'...");
    $card = json_encode($card, JSON_UNESCAPED_UNICODE);
    print("\n    Sent JSON : \n'{$card}'...");

    $issue_id= -1;
    if ($dry_run) {
        $dry_run_counter += 1;
        $issue_id = $dry_run_counter;
        $issue_subject = $issue->subject;
        print("\n        $card\n");
    }else{
        $result = curl_post($cfg['redmine_root_url'] . '/issues.json', $card);
        $issue = json_decode($result, JSON_UNESCAPED_UNICODE);
        $issue_id = $issue['issue']['id'];
        $issue_subject = $issue['issue']['subject'];
        print("\n    Response JSON:\n{$result}\n    Issue ID: '{$issue_id}'\n");
    }

    if ($issue_id >= 0){
        $issue = new stdClass();

        // Due to Redmine BUG disallowing to set any status except "New" when creating cards we should update the card next to creation :(
        // Check setting for archive card
        if($crd->closed and isset($cfg['redmine_status_for_archive_card']) and $cfg['redmine_status_for_archive_card'] and isset($redmine_statuses_dict[$cfg['redmine_status_for_archive_card']])){
            $issue->status_id = $redmine_statuses_dict[$cfg['redmine_status_for_archive_card']];
        }else{
            $issue->status_id = ($crd->idList ? $status_map[$crd->idList] : $redmine_statuses_dict[$redmine_default_status]);
        }

        // Comments (from task actions)
        $actions = curl_get($cfg['trello_api_link'] . "/cards/{$crd->id}/actions?filter=commentCard,copyCommentCard" . '&token=' . $cfg['trello_token'] . '&key=' . $cfg['trello_key']);
        $actions = json_decode($actions, JSON_UNESCAPED_UNICODE);
        if($actions and is_array($actions) and count($actions)){
            $issue->notes = '';
            foreach ($actions as $index => $a) {

                if ($a['type']== "commentCard") { // for original board
                    $author_id = $a['idMemberCreator'];
                    $comment = array(
                        "date" =>       (isset($a['data']['dateLastEdited']) ? $a['data']['dateLastEdited'] : $a['date']),
                        "author" =>     ((isset($members_dict[$author_id]) and isset($cfg['username_map'][$members_dict[$author_id]])) ?
                                            $cfg['username_map'][$members_dict[$author_id]] :
                                            (isset($members_dict[$author_id]) ?
                                                $members_dict[$author_id] : ''
                                            )
                                        ),
                        "text" =>       $a['data']['text'],
                    );
                }elseif ($a['type'] == "copyCommentCard") { // for copied board
                    $author_id = $a['data']['idOriginalCommenter'];
                    $comment = array(
                        "date" =>       (isset($a['data']['dateLastEdited']) ? $a['data']['dateLastEdited'] : $a['date']),
                        "author" =>     ((isset($members_dict[$author_id]) and isset($cfg['username_map'][$members_dict[$author_id]])) ?
                            $cfg['username_map'][$members_dict[$author_id]] :
                            (isset($members_dict[$author_id]) ?
                                $members_dict[$author_id] : ''
                            )
                        ),
                        "text" =>       $a['data']['text'],
                    );
                }

                $issue->notes .= ($comment['author'] ? "**{$comment['author']}**" :  '') . ' _(' . date("Y-m-d H:i:s", strtotime($comment['date'])) . ' GMT)_:' . "\n" . $comment['text'] . "\n***\n\n";
                print("\n        Importing comment " . ($index + 1) . " of " . count($actions) . " for Issue: '{$issue_subject}'...");
            }
        }

        $update = new stdClass();
        $update->issue = $issue;
        $update = json_encode($update, JSON_UNESCAPED_UNICODE);
        if ($dry_run) {
            print("\n            $update\n\n");
        }else{
            $result = curl_put($cfg['redmine_root_url'] . "/issues/{$issue_id}.json", $update);
        }
    }

    // Attachments
    if(isset($crd->attachments) and is_array($crd->attachments) and count($crd->attachments)){
        foreach ($crd->attachments as $index => $attachment){
            if($attachment->isUpload ) { // If attachment is regular attachment
                print("\n        Downloading attachment " . ($index + 1) . " of " . count($crd->attachments) . " for Issue: '{$issue_subject}'...");
                $file = file_get_contents($attachment->url);
                if ($dry_run) {
                    print("\n            Got file named '{$attachment->name}' from '{$attachment->url}' size " . strlen($file) / 1024 . " KB\n");
                } else {

                    print("\n            Uploading attachment " . ($index + 1) . " of " . count($crd->attachments) . " for Issue: '{$issue_subject}'...");
                    $redmine_attachment = curl_post_file($cfg['redmine_root_url'] . '/uploads.json', $file);
                    if ($redmine_attachment) {
                        $redmine_attachment = json_decode($redmine_attachment, JSON_UNESCAPED_UNICODE);
                        $upload = new stdClass();
                        $upload->token = $redmine_attachment['upload']['token'];
                        $upload->filename = $attachment->name;
                        $upload->content_type = $attachment->mimeType;

                        $issue = new stdClass();
                        $issue->description = $desc;
                        $issue->uploads[] = $upload;
                        $redmine_attachment_member = (
                        (
                            isset($members_dict[$attachment->idMember]) and isset($cfg['username_map'][$members_dict[$attachment->idMember]])
                        ) ?
                            $cfg['username_map'][$members_dict[$attachment->idMember]]
                            :
                            (
                            isset($members_dict[$attachment->idMember]) ? $members_dict[$attachment->idMember] : ''
                            )
                        );
                        $issue->notes = 'Файл добавил' . ($redmine_attachment_member ? " **{$redmine_attachment_member }**" : 'и') . ' _(' . date("Y-m-d H:i:s",
                                strtotime($attachment->date)) . ' GMT)_';

                        $update = new stdClass();
                        $update->issue = $issue;
                        print("\n                Importing attachment " . ($index + 1) . " of " . count($crd->attachments) . " for Issue: '{$issue_subject}'...");
                        $update = json_encode($update, JSON_UNESCAPED_UNICODE);
                        $result = curl_put($cfg['redmine_root_url'] . "/issues/{$issue_id}.json", $update);
                    }
                }
            }else{ // If attachment is a link
                print("\n        Found attachment-link " . ($index + 1) . " of " . count($crd->attachments) . " for Issue: '{$issue_subject}'...");
                if ($dry_run) {
                    print("\n            Got link named '{$attachment->name}' from '{$attachment->url}'\n");
                }else{
                    $redmine_attachment_member = (
                    (
                        isset($members_dict[$attachment->idMember]) and isset($cfg['username_map'][$members_dict[$attachment->idMember]])
                    ) ?
                        $cfg['username_map'][$members_dict[$attachment->idMember]]
                        :
                        (
                        isset($members_dict[$attachment->idMember]) ? $members_dict[$attachment->idMember] : ''
                        )
                    );

                    $issue = new stdClass();
                    $desc .= "\n\n Link added: [{$attachment->url}]($attachment->url)" . ($redmine_attachment_member ? " **{$redmine_attachment_member }**" : '') . ' _(' . date("Y-m-d H:i:s",
                            strtotime($attachment->date)) . ' GMT)_';
                    $issue->description = $desc;
                    $issue->notes = 'Link added by' . ($redmine_attachment_member ? " **{$redmine_attachment_member }**" : 'и') . ' _(' . date("Y-m-d H:i:s",
                            strtotime($attachment->date)) . ' GMT)_' . "[{$attachment->url}]($attachment->url)";

                    $update = new stdClass();
                    $update->issue = $issue;
                    print("\n                Importing attachment " . ($index + 1) . " of " . count($crd->attachments) . " for Issue: '{$issue_subject}'...");
                    $update = json_encode($update, JSON_UNESCAPED_UNICODE);
                    $result = curl_put($cfg['redmine_root_url'] . "/issues/{$issue_id}.json", $update);
                }
            }
        }
    }

    print("\n");
}

print("\n\n┌──────────────────────────────────────────────────────────────────────────┐\n");
print("│                                D O N E !                                 │\n");
print("└──────────────────────────────────────────────────────────────────────────┘\n");