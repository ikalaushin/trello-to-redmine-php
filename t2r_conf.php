<?php
/**
 * t2r_conf.php
 *
 * Trello to Redmine migrating script configuration file
 *
 * Created by PhpStorm.
 * User: ikalaushin (ikalaushin@gmail.com)
 * Date: 02.12.2016
 * Time: 17:07
 */

// This is the Trello to Redmine migrating script configuration file.
// All data here is exemplary, you need to fill it properly for the script to work!

// Trello configuration

$cfg['trello_board_id'] = '';                                                   // As found on the board in Menu -> More -> Link to this board (last part like jQBh4WG1).
$cfg['trello_token'] = '';
$cfg['trello_key'] = '';
$cfg['trello_api_link'] = 'https://api.trello.com/1';
$cfg['trello_params'] = 'fields=all&actions=all&action_fields=all&actions_limit=1000&cards=all&card_fields=all&card_attachments=true&lists=all&list_fields=all&members=all&member_fields=all&checklists=all&checklist_fields=all&labels=all&label_fields=all'; // Trello query string

// Redmine configuration
$cfg['redmine_root_url'] = 'https://redmine.you-cm.com';                        // Your Redmine installation URL like https://redmine.yourdomain.com
$cfg['redmine_project_identifier'] = 'you-bugs';                                // As appears in the Redmine URLs.
$cfg['redmine_default_user'] = 'Unknown User';                                  // Trello cards which are unassigned or whose assignee does not have a mapping in username_map will be assigned to this Redmine user.
$cfg['redmine_verify_certificates'] = False;                                    // Whether to verify SSL certificates.
$cfg['redmine_api_key'] = '';                                                   // Redmine REST API key. See: http://www.redmine.org/projects/redmine/wiki/Rest_api#Authentication

// Default status
$cfg['redmine_default_status'] = "New";

// Status for archive card
$cfg['redmine_status_for_archive_card'] ='Closed';

// ID for default Redmine tracker
$cfg['redmine_default_tracker_id'] = 2;

// Words in Trello card  title for seting Redmine tracker 'Error' (1) for redmine task
$cfg['redmine_tracker_error_alias'] = array(
    'BUG',
    'ERROR',
    '',
);

// Words in Trello card  title for seting Redmine tracker 'Feature' (2) for redmine task
$cfg['redmine_tracker_feature_alias'] = array(
    'Feature',
    'Extension',
);

// Words in Trello card  title for seting Redmine tracker 'Support' (3) for redmine task
$cfg['redmine_tracker_support_alias'] = array(
    'Support',
    'Report',
);

// Trello card label to Redmine priority map.
$cfg['label_map'] = array(
    ''                                                => 'Normal',    // Default for no label.
    'On HOLD (do not move)'                           => 'Low',
    'IMPORTANT'                                       => 'High',
);

// Trello-to-Redmine username map. Both are displayed names, *not* logins or IDs! Redmine names are resolved to user IDs behind the scenes.
$cfg['username_map'] = array(
//    'Trello User'=>        'Redmine User',
);

// Trello list to Redmine status map. Redmine statuses are displayed names, *not* IDs! Statuses are resolved to IDs behind the scenes.
$cfg['list_map'] = array(
    'TODO'                                                          => 'New',
    'In progress'                                                   => 'In progress',
    'Done'                                                          => 'Done',
    'Testing'                                                       => 'Testing',
    'Closed'                                                        => 'Closed',
);
